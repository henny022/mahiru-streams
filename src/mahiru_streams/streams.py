import twitch_helix.models
from twitch_helix import HelixClient


class Stream:
    user_name: str
    url: str
    title: str
    game: str
    thumbnail: str
    pfp: str
    color: int
    language: str

    __key = object()

    @classmethod
    def from_twitch(cls, stream: twitch_helix.models.Stream, user: twitch_helix.models.User):
        self = cls(cls.__key)
        self.user_name = stream.user_name
        self.url = f'https://twitch.tv/{stream.user_login}'
        self.title = stream.title
        self.game = stream.game_name
        self.thumbnail = stream.get_thumbnail_url(16 * 50, 9 * 50)
        self.pfp = user.profile_image_url
        self.color = 1369976
        self.language = stream.language
        return self

    def __init__(self, key):
        assert key == self.__key


class StreamManager:
    def __init__(self, client_id, client_secret):
        self.helix = HelixClient(
            client_id,
            client_secret,
        ).with_app_token()

    async def get_person_stream(self, name):
        pass

    async def get_game_streams(self, game, tags=None) -> list[Stream]:
        if tags is None:
            tags = []
        streams = self.helix.get_streams(game_id=game)
        streams = [stream for stream in streams if stream.game_id == game]
        return [
            Stream.from_twitch(stream, self.helix.get_user(id=stream.user_id))
            for stream in streams
            if not tags or any([tag.lower() in [
                stag.lower() for stag in stream.tags
            ] for tag in tags])
        ]
