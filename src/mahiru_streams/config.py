import json
from dataclasses import dataclass
from typing import List, Union


class Mode:
    GAME = 'game'
    PERSON = 'person'


@dataclass
class GameConfig:
    game_id: str
    tags: List[str]
    people: List[str]

    def to_json(self):
        return self.__dict__

    @classmethod
    def from_json(cls, **kwargs):
        return cls(**kwargs)


@dataclass
class PersonConfig:
    login: str

    def to_json(self):
        return self.__dict__

    @classmethod
    def from_json(cls, **kwargs):
        return cls(**kwargs)


@dataclass
class ChannelConfig:
    name: str
    id: int
    delete_after: int
    mode: str
    data: List[Union[GameConfig, PersonConfig]]

    def to_json(self):
        return self.__dict__

    @classmethod
    def from_json(
            cls,
            name: str,
            id: int,
            delete_after: int,
            mode: str,
            data: List[dict]
    ):
        if mode == Mode.GAME:
            data = [GameConfig.from_json(**c) for c in data]
        elif mode == Mode.PERSON:
            data = [PersonConfig.from_json(**c) for c in data]
        return cls(name, id, delete_after, mode, data)


@dataclass
class Config:
    discord_token: str
    twitch_client_id: str
    twitch_client_secret: str
    supermods: List[int]
    interval: int
    channels: List[ChannelConfig]

    def to_json(self):
        return self.__dict__

    @classmethod
    def from_json(
            cls,
            discord_token: str,
            twitch_client_id: str,
            twitch_client_secret: str,
            supermods: List[int],
            interval: int,
            channels: List[dict]
    ):
        channels = [ChannelConfig.from_json(**c) for c in channels]
        return cls(discord_token, twitch_client_id, twitch_client_secret, supermods, interval, channels)


def load_config(filename):
    with open(filename) as configfile:
        data = json.load(configfile)
    return Config.from_json(**data)


def save_config(config, filename):
    with open(filename, 'w') as configfile:
        json.dump(config.to_json(), configfile, indent=2, default=lambda o: o.to_json)
