import argparse
import logging
import re
import sys
from typing import Optional

import discord
import mahiru_streams.config
from discord.ext import tasks
from mahiru_streams.config import load_config, GameConfig
from mahiru_streams.streams import StreamManager, Stream

author_pattern = re.compile(r'(?P<login>\w+) is now live on Twitch!')


def extract_login(message: discord.Message) -> Optional[str]:
    if not message.embeds:
        return None
    if message.embeds[0].author.name is None:
        return None
    match = author_pattern.match(message.embeds[0].author.name)
    if not match:
        return None
    return match.group('login')


language_flags = {
    'en': '🇬🇧',  # '🇺🇸'
    'de': '🇩🇪',
    'fr': '🇫🇷',
    'es': '🇪🇸',
}


def create_embed(stream: Stream):
    embed = discord.Embed(
        title=f'{stream.user_name} is live: {stream.title}',
        url=stream.url,
        color=stream.color,
    )
    if stream.language not in language_flags:
        print(f'WARNGING: untested language {stream.language}')
        language_emote = ''
    else:
        language_emote = language_flags[stream.language]
    embed.set_footer(
        text=f'Playing {stream.game} {language_emote}',
    )
    embed.set_thumbnail(
        url=stream.thumbnail,
    )
    embed.set_author(
        name=f'{stream.user_name} is now live on Twitch!',
        url=stream.url,
        icon_url=stream.pfp,
    )
    return embed


class MahiruStreams(discord.Client):
    def __init__(self, config_filename, log_level, *args, **kwargs):
        super(MahiruStreams, self).__init__(*args, **kwargs)
        self.config_file = config_filename
        self.config = load_config(self.config_file)
        self.stream_manager = StreamManager(self.config.twitch_client_id, self.config.twitch_client_secret)
        self.logger = logging.getLogger('mahiru')
        self.logger.setLevel(log_level)
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(
            '{asctime} [{levelname: ^8}] {name}: {message}',
            '%Y-%m-%d %H:%M:%S',
            style='{'
        ))
        self.logger.addHandler(handler)
        self.update_streams.change_interval(seconds=self.config.interval)

    async def on_ready(self):
        print(f'logged in as {self.user}')
        print('servers available:')
        for guild in self.guilds:
            print(f'- {guild.name}')
        self.update_streams.start()

    @tasks.loop(seconds=10)
    async def update_streams(self):
        for channel_config in self.config.channels:
            try:
                channel = self.get_channel(channel_config.id)
                self.logger.info(f'channel {channel.guild.name}#{channel.name}')
                messages: list[discord.Message] = [message async for message in channel.history()]
                embeds = {key: value for key, value in
                          [(extract_login(message), message) for message in messages if
                           extract_login(message) is not None]}
                if channel_config.mode == mahiru_streams.config.Mode.GAME:
                    config: GameConfig = channel_config.data[0]
                    streams = await self.stream_manager.get_game_streams(config.game_id, config.tags)
                    streams = {stream.user_name: stream for stream in streams}
                    new_streams = [streams[name] for name in streams if name not in embeds]
                    old_embeds = [embeds[name] for name in embeds if name not in streams]
                    existing_embeds = [(embeds[name], streams[name]) for name in embeds if name in streams]
                    for stream in new_streams:
                        await channel.send(embed=create_embed(stream))
                    for embed in old_embeds:
                        embed: discord.Message
                        await embed.delete()
                    for embed, stream in existing_embeds:
                        embed: discord.Message
                        await embed.edit(embed=create_embed(stream))
            except Exception as e:
                self.logger.error(
                    f'exception processing {channel_config.name}'
                    f'({channel_config.id})',
                    exc_info=e,
                )

    def get_channel_config(self, channel_id) -> Optional[mahiru_streams.config.ChannelConfig]:
        for channel in self.config.channels:
            if channel.id == channel_id:
                return channel
        return None

    def save_config(self):
        mahiru_streams.config.save_config(self.config, self.config_file)

    def run(self, *args, **kwargs):
        super(MahiruStreams, self).run(self.config.discord_token, *args, **kwargs)


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--config-file', required=True)
    parser.add_argument('-v', action='count')
    return parser.parse_args(argv)


def main(argv):
    print('Mahiru Streams')
    args = parse_args(argv)
    log_level = logging.WARNING
    if args.v > 0:
        log_level = logging.INFO
    if args.v > 1:
        log_level = logging.DEBUG
    intents = discord.Intents.default()
    intents.message_content = True
    client = MahiruStreams(args.config_file, log_level, intents=intents)
    client.run()


async def spam(channel: discord.TextChannel):
    while True:
        message = await channel.send('spam')
        await message.delete()


if __name__ == '__main__':
    main(sys.argv[1:])
